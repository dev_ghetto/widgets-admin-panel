import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home.vue'
import Profile from '@/pages/Profile.vue'
import Company from '@/pages/Company.vue'
import Orders from '@/pages/Orders.vue'
import Order from '@/pages/Order.vue'
import Services from '@/pages/Services.vue'
import Service from '@/pages/Service.vue'
import Employees from '@/pages/Employees.vue'
import Employee from '@/pages/Employee.vue'
import SignUp from '@/pages/SignUp.vue'
import SignIn from '@/pages/SignIn.vue'
import Confirm from '@/pages/Confirm.vue'
import Widget from '@/pages/Widget.vue'
import Bots from '@/pages/Bots.vue'
import Schedule from '@/pages/Schedule/index.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      children: [
        {
          path: 'profile',
          component: Profile,
        },
        {
          path: 'company',
          component: Company,
        },
        {
          path: 'orders',
          component: Orders,
        },
        {
          path: 'orders/new',
          component: Order,
        },
        {
          path: 'services',
          component: Services,
        },
        {
          path: 'services/:id',
          component: Service,
        },
        {
          path: 'employees',
          component: Employees,
        },
        {
          path: 'employees/:id',
          component: Employee,
        },
        {
          path: 'widget',
          component: Widget,
        },
        {
          path: 'schedule',
          component: Schedule,
        },
        {
          path: 'bots',
          component: Bots,
        },
      ]
    },
    {
      path: '/signup',
      name: 'signup',
      component: SignUp,
    },
    {
      path: '/signin',
      name: 'signin',
      component: SignIn,
    },
    {
      path: '/registration/confirm',
      name: 'confirm',
      component: Confirm,
    },
  ]
})
