import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isAuthenticated: false,
    globalLoading: true,
    user: {
      id: '',
      name: '',
      email: '',
      companyId: '',
      organisationId: '',
    }
  },
  mutations: {
    changeAuthAccess(state, access) {
      return state.isAuthenticated = access;
    },
    setUser(state, payload) {
      return state.user = payload;
    },
    setCompany(state, payload) {
      return state.user.company = payload;
    },
    setGlobalLoading(state, payload) {
      return state.globalLoading = payload;
    },
  },
})
