import Vue from 'vue'
import './plugins/vuetify'
import router from './plugins/router'
import store from './plugins/store'
import App from './App.vue'
import Clipboard from 'v-clipboard'
import VueTheMask from 'vue-the-mask'
import Vuelidate from 'vuelidate'

Vue.use(Clipboard);
Vue.use(VueTheMask);
Vue.use(Vuelidate);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
