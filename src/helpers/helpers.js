import moment from 'moment';
import Vue from 'vue';

export const getToken = () => localStorage.getItem('access_token')

export const getHeaders = () => ({
  headers: getToken() ? {
    Authorization: `Bearer ${getToken()}`,
  } : null
});

export const getFormDataHeaders = () => ({
  headers: getToken() ? {
    'Content-Type': 'multipart/form-data',
    Authorization: `Bearer ${getToken()}`,
  } : null
});

export const formatDatePickerToDate = (date) => {
  return Math.ceil(moment(date).unix() / 86400);
};

export const formatSecondsToDateByUTC = (seconds, format) => {
  return moment().utc().startOf('day').seconds(seconds).local().format(format);
};

export const formatTimeToSecondsByUTC = (value) => {
  const utcTime = moment(value, 'HH:mm')
    .utc()
    .format('HH:mm');
  return moment(utcTime, 'HH:mm').diff(moment().startOf('day'), 'seconds');
};

const eventHub = new Vue();

Vue.prototype.$eventHub = eventHub;

export {eventHub};