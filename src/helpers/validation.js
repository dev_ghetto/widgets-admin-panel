const phoneNumberRegexp = new RegExp("^\\+7\\s\\(\\d{3}\\)\\s\\d{3}-\\d{2}-\\d{2}$");
const emailRegexp = new RegExp("[\\S]+@[\\S]+\\.[\\S]+$");
const telegramRegexp = new RegExp("^[a-z0-9][a-z-0-9_]{3,16}[a-z0-9]$");
const urlPathRegexp = new RegExp("^[a-z0-9][a-z-0-9_-]{3,16}[a-z0-9]$");
const passwordRegexp = new RegExp("^(?=.*\\d)(?=.*[a-zA-Z])[a-zA-Z\\d#$%&'()*+,\\-.\\\\/:;<>=?@\\[\\]{}^_`~]{6,20}$");
const time24Regexp = new RegExp("^(2[0-3]|[01][0-9]):([0-5]?[0-9])$");

export function phoneNumber(value) {
    if(!value) {
        return true;
    }

    return phoneNumberRegexp.test(value);
}

export function email(value) {
    if(!value) {
        return true;
    }

    return emailRegexp.test(value);
}

export function telegramLogin(value) {
    if(!value) {
        return true;
    }

    return telegramRegexp.test(value);
}

export function urlPath(value) {
    if(!value) {
        return true;
    }

    return urlPathRegexp.test(value);
}

export function password(value) {
    if(!value) {
        return true;
    }

    return passwordRegexp.test(value);
}

export function time24(value) {
    if(!value) {
        return true;
    }

    return time24Regexp.test(value);
}

export async function validate(field) {
    field.$touch();
    await _validations(field);
    return field.$invalid;
}

function _validations(field) {
    return new Promise(resolve => {
        if (field.$error || !field.$pending) {
            return resolve()
        }
        let poll = setInterval(() => {
            if (!field.$pending) {
                clearInterval(poll);
                resolve()
            }
        }, 200)
    })
}