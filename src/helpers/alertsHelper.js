import {eventHub} from "@/helpers/helpers";

export function addSuccessAlert(message = 'Успешно') {
    eventHub.$emit("addAlert", {
        type: "success",
        message
    });
}

export function addErrorAlert(message = 'Ошибка') {
    eventHub.$emit("addAlert", {
        type: "error",
        message
    });
}

export function addBadRequestAlert() {
    eventHub.$emit("addAlert", {
        type: "error",
        message: "Проверьте правильность введенных данных"
    })
}