module.exports = {
  configureWebpack: {},
  devServer: {
    proxy: {
      '/api': {
        target: 'http://79.174.13.123',
      },
      '/oauth': {
        target: 'http://79.174.13.123',
      },
      '/storage': {
        target: 'http://79.174.13.123',
      },
    }
  }
}
